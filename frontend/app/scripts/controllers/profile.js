'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
        .controller('ProfileCtrl', ProfileCtrl);


/*@ngInject*/
function ProfileCtrl($scope, $http, $q, $timeout, ChatSvc, $interval, McuiModalSvc, toastr) {

    var vm = this;

    vm.user = "";

    vm.img = "";

    vm.data = {
    };

    vm.init = Init;

    vm.model = {};

    vm.fn = {         
    };

    vm.init();

    function Init() {

    }
}