(function(){

	"üse strict";

	angular.module('frontendApp')
	  .controller('ConciliadorCtrl', ConciliadorCtrl)
	  .filter('sumOfValue', function () {
	    
	    	return function (data, key) {        
		        if (angular.isUndefined(data) && angular.isUndefined(key))
		            return 0; 

		        var sum = 0; 

		        angular.forEach(data,function(value){
		            sum = sum + parseFloat(value[key]);
		        });        
		        return sum;
	    	}

		});	  


	  function ConciliadorCtrl($scope, $filter, $timeout){

	  		var vm = this;
	  		vm.products = [];
	  		vm.q = "";

	  		vm.init = Init;

	  		vm.data = {
	  			portadoras: [],
	  			transaccionesBanco: [],
	  			transaccionesPortadora: [],
	  			transaccionesBancoConciliadas: [],
	  			transaccionesPortadoraConciliadas: [],
	  			transaccionesBancoPortadoraConciliadas: []
	  		};

	  		vm.formBuscar = {
	  			portadora: {},
	  			fechaI: null,
	  			fechaF: null,
	  			fechaIBanco: null,
	  			fechaFBanco: null,
	  			fechaIPortadora: null,
	  			fechaFPortadora: null,
	  			busquedaAvanzada: false
	  		};
		

	  		vm.fn = {
	  			initPortadoras: initPortadoras,
	  			initTransacciones: initTransacciones,
	  			buscarTransacciones: buscarTransacciones,
	  			activarTab: activarTab,
	  			conciliar: conciliar,
	  			finalizar: finalizar,
	  			eliminarConciliacionBanco: eliminarConciliacionBanco,
	  			eliminarConciliacionPortadora: eliminarConciliacionPortadora,
	  			eliminarConciliacionBancoPortadora: eliminarConciliacionBancoPortadora,
	  			validateOnCheck: validateOnCheck,
	  			resetModalObservacion: resetModalObservacion   
	  		}

	  		vm.display = {
	  			
	  			stepOne: true,
	  			stepTwo: false,
	  			stepThree: false,

	  			//-----------------
	  			
	  			tabConciliar: true,

	  			//-----------------

	  			btnConciliar: false
	  		}

	  		vm.messages = {
	  			noresult: "No se encontrarion resultados"
	  		}

	  		vm.trxobservacion = "SIN OBSRVACIONES";

	  		function Init(){
	  			vm.fn.initPortadoras();
	  			vm.fn.initTransacciones();
	  		}

	  		// ------- INIT

	  		vm.init();

	  		//-------- FUNCTIONS

	  		function initPortadoras(){
	  			for (var i = 1; i < 4;  i++) 
	  				vm.data.portadoras.push(new PORTADORA({id: i, nombre: "PORTADORA-" + i}));
	  		}

	  		function initTransacciones(){
	  			for (var i = 1; i < 15;  i++){
	  				if(i%2 == 0)
	  					vm.data.transaccionesBanco.push(new TRANSACCION_BANCO({id: i, nombre: "BANCO-TX-" + i, monto: 2 * i * 1000}));
	  				else
	  					vm.data.transaccionesPortadora.push(new TRANSACCION_PORTADORA({id: i, nombre: "PORTADORA-TX-" + i, monto: 2 * i * 1000}));
	  			}	  				
	  		}	  		

	  		function buscarTransacciones(){
	  			vm.display.stepOne = false;
	  			vm.display.stepTwo = true;
	  		}

	  		function activarTab(param){
	  			vm.display.tabConciliar = param;
	  		}

	  		function conciliar(){
	  			//
	  			//CONCILIAR LOS DATOS SELECCIONADOS

	  			var validarBanco = false,
	  				validarPortadora = false,
	  				validarBancoPortadora = false,
	  				validar = isValidOperaration();

	  			var trxBancoChecked = $filter("filter")(vm.data.transaccionesBanco, {active: true}),	
	  				trxPortadoraChecked = $filter("filter")(vm.data.transaccionesPortadora, {active: true});

	  			var cTrxBancoChecked = trxBancoChecked.length,
	  				cTrxPortadoraChecked = trxPortadoraChecked.length;

	  			//
	  			// Check Observacion	
	  			var observacion = vm.trxobservacion;
	  			if(!observacion || observacion == "")
	  				return;	  			

	  			//
	  			// 1. Verificar Banco - Portadora

	  			if(cTrxBancoChecked == 1 && cTrxPortadoraChecked == 1){
	  				validarBancoPortadora = true;
	  			}

	  			//
	  			// 2. Verificar Banco

	  			if(cTrxBancoChecked >= 1 && cTrxPortadoraChecked == 0){
	  				validarBanco = true;
	  			}

	  			//
	  			// 3. Verificar Portadora

	  			if(cTrxBancoChecked == 0 && cTrxPortadoraChecked >= 1){
	  				validarPortadora = true;
	  			}

	  			if(!validar)
	  				alert("Operación incorrecta");
	  			else{

	  				//SET BLOCKED CONCILAR CONTROL
	  				vm.display.btnConciliar = false;

	  				if(validarBancoPortadora){

	  					var banco = trxBancoChecked[0],
	  						portadora = trxPortadoraChecked[0];

	  					//CONCILIAR BANCO-PORTADORA	
	  					var cbp = conciliarBancoPortadora(banco, portadora, observacion);

	  					if(cbp){
		  					//ELIMINAR-BANCO
		  					var indexBnc = vm.data.transaccionesBanco.indexOf(banco);  
		  					vm.data.transaccionesBanco.splice(indexBnc, 1);

		  					//ELIMINAR-PORTADORA
		  					var indexPrt = vm.data.transaccionesPortadora.indexOf(portadora);  
		  					vm.data.transaccionesPortadora.splice(indexPrt, 1);
		  				}		  					
	  				}
	  				else if(validarBanco){

	  					//CONCILIAR BANCO
	  					var cb = conciliarBanco(trxBancoChecked, observacion);

	  					//ELIMINAR-BANCO
	  					if(cb){
					        angular.forEach(trxBancoChecked, function(value){
			  					var index = vm.data.transaccionesBanco.indexOf(value);  
			  					vm.data.transaccionesBanco.splice(index, 1);
					        });		  						
	  					}
  					
	  				}
	  				else if(validarPortadora){

	  					//CONCILIAR PORTADORA
	  					var cp = conciliarPortadora(trxPortadoraChecked, observacion);

	  					//ELIMINAR-PORTADORA
	  					if(cp){
					        angular.forEach(trxPortadoraChecked, function(value){
			  					var index = vm.data.transaccionesPortadora.indexOf(value);  
			  					vm.data.transaccionesPortadora.splice(index, 1);
					        });	
	  					}
	  				}
	  			}
	  			
	  		}

	  		function finalizar(){
	  			vm.display.tabConciliar = false;
	  		}

	  		function conciliarBancoPortadora(banco, portadora, observacion){
	  			if(!banco || !portadora || !observacion || observacion == "")
	  				return false;

	  			vm.data.transaccionesBancoPortadoraConciliadas.push(new TRANSACCION_BANCOPORTADORA(banco, portadora, observacion));

	  			return true;
	  		}

	  		function conciliarBanco(listadoBanco, observacion){
	  			
	  			if(!observacion || !listadoBanco || listadoBanco.length <= 0 || observacion == "")
	  				return false;

	  			angular.forEach(listadoBanco, function(value){
	  				value['observacion'] = observacion;
		            vm.data.transaccionesBancoConciliadas.push(value);
		        });

		        return true;
	  		}

	  		function conciliarPortadora(listadoPortadora, observacion){

	  			if(!observacion || !listadoPortadora || listadoPortadora.length <= 0 || observacion == "")
	  				return false;

		        angular.forEach(listadoPortadora, function(value){
		        	value['observacion'] = observacion;
		            vm.data.transaccionesPortadoraConciliadas.push(value);
		        });

		        return true;
	  		}

	  		function eliminarConciliacionBanco(banco, index){
	  			banco['active'] = false;
	  			banco['observacion'] = null;

	  			//ADD BANCO TRANSACCION
	  			vm.data.transaccionesBanco.push(banco);

	  			//ELIMINA BANCO CONCILIACION
	  			vm.data.transaccionesBancoConciliadas.splice(index, 1);
	  		}

	  		function eliminarConciliacionPortadora(portadora, index){
	  			portadora['active'] = false;
	  			portadora['observacion'] = null;

	  			//ADD PORTADORA TRANSACCION
	  			vm.data.transaccionesPortadora.push(portadora);

	  			//ELIMINA PORTADORA CONCILIACION
	  			vm.data.transaccionesPortadoraConciliadas.splice(index, 1);
	  		}	  		

	  		function eliminarConciliacionBancoPortadora(banco, portadora, index){
	  			banco['active'] = false;
	  			portadora['active'] = false;

	  			banco['observacion'] = null;
	  			portadora['observacion'] = null;

	  			//ADD BANCO TRANSACCION
	  			vm.data.transaccionesBanco.push(banco);

	  			//ADD PORTADORA TRANSACCION
	  			vm.data.transaccionesPortadora.push(portadora);

	  			//ELIMINA BANCO-PORTADORA CONCILIACION
	  			vm.data.transaccionesBancoPortadoraConciliadas.splice(index, 1);
	  		}  		



	  		function isValidOperaration(){
	  			var validar = false;

	  			var trxBancoChecked = $filter("filter")(vm.data.transaccionesBanco, {active: true}),	
	  				trxPortadoraChecked = $filter("filter")(vm.data.transaccionesPortadora, {active: true});

	  			var cTrxBancoChecked = trxBancoChecked.length,
	  				cTrxPortadoraChecked = trxPortadoraChecked.length;	  			
	  			
	  			//
	  			// 1. Verificar Banco - Portadora

	  			if(cTrxBancoChecked == 1 && cTrxPortadoraChecked == 1){
	  				validar = true;
	  			}

	  			//
	  			// 2. Verificar Banco

	  			if(cTrxBancoChecked >= 1 && cTrxPortadoraChecked == 0){
	  				validar = true;
	  			}

	  			//
	  			// 3. Verificar Portadora

	  			if(cTrxBancoChecked == 0 && cTrxPortadoraChecked >= 1){
	  				validar = true;
	  			}	

	  			return validar;  
	  		}

	  		function validateOnCheck(){				
				var isvalid = isValidOperaration();
				vm.display.btnConciliar = (!isvalid) ? false : true;
	  		}  

	  		function resetModalObservacion(){

	  		}			  		

	  		//-------- MODEL

	  		function TRANSACCION_BANCO(model){
	  			this.id = model.id || null;
	  			this.idCliente = model.idCliente || "BFGT-65982-*KH45";
	  			this.operacion = model.operacion || "BP-XK-58";
	  			this.monto = model.monto || 200000;
	  			this.fecha = model.fecha || new Date();
	  			
	  			//
	  			//VISUAL PROPERTIES
	  			
	  			this.active = false;
	  			this.observacion = model.observacion || null;
	  		}

	  		function TRANSACCION_PORTADORA(model){
	  			this.id = model.id || null;
	  			this.idCliente = model.idCliente || "TPFGT-65982-*KH45";
	  			this.operacion = model.operacion || "TOP-XK-58";
	  			this.monto = model.monto || 200000;
	  			this.fecha = model.fecha || new Date();

	  			//
	  			//VISUAL PROPERTIES
	  			
	  			this.active = false;
	  			this.observacion = model.observacion || null;
	  		}		  			  		

	  		function PORTADORA(model){
				this.id = model.id || null;
				this.nombre = model.nombre || "";
			}

	  		function TRANSACCION_BANCOPORTADORA(banco, portadora, observacion){
	  			this.banco = banco || null;
	  			this.portadora = portadora || null;
	  			this.observacion = observacion || null;
	  		}

	  		//------- UTIL

			$('#modalObservacion').on('show.bs.modal', function (event) {
				$timeout(function(){
					vm.trxobservacion = "SIN OBSERVACIONES";
				}, 100)				
			}) 


			//------------------ PICKER

    // date picker
    vm.picker1 = {
        date: new Date('2015-03-01T00:00:00Z'),
        datepickerOptions: {
            showWeeks: false,
            startingDay: 1,
            dateDisabled: function(data) {
                return (data.mode === 'day' && (new Date().toDateString() == data.date.toDateString()));
            }
        }
    };

    // time picker
    vm.picker2 = {
        date: new Date('2015-03-01T12:30:00Z'),
        timepickerOptions: {
            readonlyInput: false,
            showMeridian: false
        }
    };

    // date and time picker
    vm.picker3 = {
        date: new Date()
    };

    // min date picker
    vm.picker4 = {
        date: new Date(),
        datepickerOptions: {
            maxDate: null
        }
    };

    // max date picker
    vm.picker5 = {
        date: new Date(),
        datepickerOptions: {
            minDate: null
        }
    };

    // set date for max picker, 10 days in future
    vm.picker5.date.setDate(vm.picker5.date.getDate() + 10);

    // global config picker
    vm.picker6 = {
        date: new Date()
    };

    // dropdown up picker
    vm.picker7 = {
        date: new Date()
    };

    // view mode picker
    vm.picker8 = {
        date: new Date(),
        datepickerOptions: {
            mode: 'year',
            minMode: 'year',
            maxMode: 'year'
        }
    };

    // dropdown up picker
    vm.picker9 = {
        date: null
    };

    // min time picker
    vm.picker10 = {
        date: new Date('2016-03-01T09:00:00Z'),
        timepickerOptions: {
            max: null
        }
    };

    // max time picker
    vm.picker11 = {
        date: new Date('2016-03-01T10:00:00Z'),
        timepickerOptions: {
            min: null
        }
    };

    // button bar
    vm.picker12 = {
        date: new Date(),
        buttonBar: {
            show: true,
            now: {
                show: true,
                text: 'Now!'
            },
            today: {
                show: true,
                text: 'Today!'
            },
            clear: {
                show: false,
                text: 'Wipe'
            },
            date: {
                show: true,
                text: 'Date'
            },
            time: {
                show: true,
                text: 'Time'
            },
            close: {
                show: true,
                text: 'Shut'
            }
        }
    };

    // when closed picker
    vm.picker13 = {
        date: new Date(),
        closed: function(args) {
            vm.closedArgs = args;
        }
    };

    // saveAs - ISO
    vm.picker14 = {
        date: new Date().toISOString()
    }

    vm.openCalendar = function(e, picker) {
        vm[picker].open = true;
    };

    // watch min and max dates to calculate difference
    var unwatchMinMaxValues = $scope.$watch(function() {
        return [vm.picker4, vm.picker5, vm.picker10, vm.picker11];
    }, function() {
        // min max dates
        vm.picker4.datepickerOptions.maxDate = vm.picker5.date;
        vm.picker5.datepickerOptions.minDate = vm.picker4.date;

        if (vm.picker4.date && vm.picker5.date) {
            var diff = vm.picker4.date.getTime() - vm.picker5.date.getTime();
            vm.dayRange = Math.round(Math.abs(diff/(1000*60*60*24)))
        } else {
            vm.dayRange = 'n/a';
        }

        // min max times
        vm.picker10.timepickerOptions.max = vm.picker11.date;
        vm.picker11.timepickerOptions.min = vm.picker10.date;
    }, true);


    // destroy watcher
    $scope.$on('$destroy', function() {
        unwatchMinMaxValues();
    });


	  }

})();