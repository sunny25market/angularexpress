'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
        .controller('ChatCtrl', ChatCtrl)
        .directive('inputChat', function() {

            return {
                restrict: 'A',
                link: function(scope, element, attrs) {

                    var inputRef = attrs.inputRef || 'chat-input-text';

                    var input = '<input type="text" id="' + inputRef + '" style="" />';
                    var inputID = "#" + inputRef;

                    $(input).insertAfter(element);

                    $(element).bind('input propertychange', function(evt) {

                        var element = $(this);

                        var prev = $(inputID);
                        var nextVal = element[0].textContent;
                        var prevVal = prev.val();

                        var clientHeight = element[0].clientHeight;
                        var scrollHeight = element[0].scrollHeight;

                        var hasScroll = scrollHeight > clientHeight;
                        var initialHeight = 30;
                        var maxHeight = 30 * 3;

                        if (nextVal.length > prevVal.length) {

                            var calulatedHeight = parseInt(clientHeight) + parseInt(initialHeight);

                            while (hasScroll && clientHeight <= maxHeight) {
                                element.css({'height': calulatedHeight});

                                hasScroll = element[0].scrollHeight > element[0].clientHeight;
                                clientHeight = element[0].clientHeight;
                                calulatedHeight = parseInt(clientHeight) + parseInt(initialHeight);
                            }

                        } else if (nextVal.length == prevVal.length) {
                            console.log("no change..");
                        } else if (nextVal.length < prevVal.length) {

                            if (hasScroll) {


                            } else {

                                var flagStop = false;

                                while (!hasScroll && clientHeight >= initialHeight) {

                                    var calulatedHeight = parseInt(clientHeight) - parseInt(initialHeight);

                                    element.css({'height': calulatedHeight});

                                    //console.log("recalculando...");

                                    hasScroll = element[0].scrollHeight > element[0].clientHeight;
                                    clientHeight = element[0].clientHeight;

                                    if (hasScroll) {
                                        //console.log("reestableciendo...");

                                        var newHeight = parseInt(calulatedHeight) + (parseInt(initialHeight));

                                        if (clientHeight < initialHeight) {
                                            element.css({'height': initialHeight});
                                        } else {
                                            element.css({'height': newHeight});
                                        }

                                        flagStop = true;
                                    }
                                }
                            }
                        }
                        prev.val(nextVal);
                    });
                }
            };
        })
        .service('ChatSvc', ChatSvc)
        .config(function(uiSelectConfig) {
            uiSelectConfig.theme = 'bootstrap'; // select2 | bootstrap | selectize 
            uiSelectConfig.resetSearchInput = false;
            uiSelectConfig.appendToBody = false;
            uiSelectConfig.removeSelected = true;
        });


/*@ngInject*/
function ChatCtrl($scope, $http, $q, $timeout, socket, ChatSvc, $interval, McuiModalSvc, toastr) {

    var vm = this;

    vm.user = "";

    vm.img = "";

    vm.data = {
        messagesoffline: [],
        messagesonline: [],
        users: [],
    };

    vm.rating = 4;

    vm.init = Init;

    vm.interval = "1m";

    vm.onSearch = function(interval) {
        console.log(interval);
    }

    vm.model = {};

    vm.fn = {
        inputChange: function() {

            var element = $('#chat-input');
            var prev = element.data('val');
            var next = element.val();
            console.log(prev);

        },
        loadMessages: loadMessages,
        renderMessage: renderMessage,
        sendMessage: sendMessage,
        notifyTyping: notifyTyping,
        getUserSelected: getUserSelected,
        onSelect: function(value, text, event) {
        },
        openNotification: function(type){
            if(type == 'success')
                toastr.success('Datos guardados correctamente.');
            else if(type == 'warning')
                toastr.warning('Posible dato duplicado.');
            else if(type == 'info')
                toastr.info('Boleto pagable.');
            else if(type == 'error')
                toastr.error('Ha ocurrido un error intentando actualizar los datos de la portadora.');
        },
        openModal: openModal,
        closeAllModal: closeAllModal,
        parseHtml: function (string) {

            var filters = ['url'];

            var str = string;//"http://telemundo.com and https://www.facebook.com or http://telemundo.com";

            //URL
            if (str && filters.indexOf('url') != -1)
                str = vm.fn.urlfy(str);

            return str;
        },
        urlfy: function (string) {

            var str = string;

            //starting with http://, https://, or ftp://
            var pattern_1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
            str = str.replace(pattern_1, '<a href="$1" target="_blank">$1</a>');

            //starting with "www." (without // before it, or it'd re-link the ones done above).
            var pattern_2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
            str = str.replace(pattern_2, '<a href="http://$2" target="_blank">$2</a>');

            //email addresses to mailto:: links.
            var pattern_3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
            str = str.replace(pattern_3, '<a href="mailto:$1">$1</a>');

            return str;
        }                
    };

    function openModal(){

        var cloneScope = $scope.$new();
        cloneScope.message = "this is a dummie test";
        cloneScope.title = "Bienvenido";

        McuiModalSvc.show({
            scope:cloneScope
        })
    }

    function closeAllModal(){
        McuiModalSvc.closeAll();
    }

    socket.on('msg:wellcome', function(data) {

        angular.forEach(data, function(value, i) {

            var custom = {sent: false};
            angular.extend(custom, value);
            //
            //Message
            vm.fn.renderMessage(custom);
        });
    })

    socket.on('msg:new', function(data) {
        var custom = {sent: false};
        angular.extend(custom, data);
        //
        //Message
        vm.fn.renderMessage(custom);
    })

    socket.on('msg:typing', function(data) {
        $timeout(function() {
            var typing = {author: '', message: 'is typing...'};
            angular.extend(typing, data);
            vm.chat.typing = typing;
        })
    })

    vm.chat = {
        message: "",
        typing: {author: null, message: 'is typing...'}
    }

    vm.init();

    function Init() {
        //loadMessagesOffline();
        //loadMessagesOnline();
        initChatInput();
        initChatFile();
        initMinMaxCloseChat();
        initChatTabs();
        initChatToolsBtn();
        loadUsers();
        checkUserIsTyping();
    }

    function checkUserIsTyping() {
        $interval(function() {
            $timeout(function() {
                vm.chat.typing.author = null;
            })
        }, 2000);
    }

    function sendMessage(message) {
        var m = {author: vm.fn.getUserSelected()};
        angular.extend(m, message);
        socket.emit('msg:sent', m);

        var test = document.getElementById("test");
        var tt = test.value;
        if (!tt || tt == '' || tt == null) {
            console.log('cambo aun sin valor...');
        } else {
            console.log('test:', tt);
        }
    }

    function renderMessage(message) {

        $timeout(function() {

            message['text'] = vm.fn.parseHtml(message['text']);

            var m = new CHAT_MESSAGE(message);
            vm.data.messagesonline.push(m);
            scrollTop();
        });
    }

    function notifyTyping(data) {
        var m = {author: vm.fn.getUserSelected()};
        angular.extend(m, data);
        socket.emit('msg:typing', m);
    }

    function excel() {

        var request = {
            method: 'GET',
            url: 'http://localhost:8000/api/v1/print/excel',
            responseType: 'arraybuffer',
            dataType: 'json',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        };

        $http(request).then(function successCallback(response) {

            var url = (window.URL || window.webkitURL).createObjectURL(response);
            window.open(url, "_self");
            console.log(response);

        }, function errorCallback(response) {
            console.log("ERR: ", response);
        });
    }

    function initChatInput() {
        initChatInputOffline();
        initChatInputOnline();
    }

    function initMinMaxCloseChat() {

        var minMaxContainer = $('.chat-container');
        var chatContainer = $('#chat');
        var classMaxim = 'chat-maximized';
        var classMinim = 'chat-minimized';

        $('.min-btn').on('click', function(evt) {
            $(this).toggleClass(classMinim);
            chatContainer.toggleClass(classMinim);
            if ($(this).hasClass(classMinim))
                minMaxContainer.slideUp();
            else
                minMaxContainer.slideDown();
        });

        $('.max-btn').on('click', function(evt) {
            minMaxContainer.slideDown();
            chatContainer.toggleClass(classMaxim);
            if (chatContainer.hasClass(classMaxim)) {
                console.log('maximized...');
                chatContainer.draggable({
                    handle: '.chat-header'
                });

                chatContainer.resizable({
                    alsoResize: ".chat-body-wrapper",
                    //minWidth: 500,
                    //minHeight: 500
                });

            } else {
                console.log('restored...');
                chatContainer.draggable("destroy");
                chatContainer.resizable("destroy");
                chatContainer.removeAttr("style");
                $('.chat-body-wrapper').removeAttr("style");
            }
        });

        $('.rem-btn').on('click', function(evt) {
            var eloverlay = '<div class="chat-end-overlay"><p>Chat finalizado</p></di>';
            if ($('.chat-end-overlay').length > 0)
                $('.chat-end-overlay').remove();
            else
                minMaxContainer.append(eloverlay);
            /*minMaxContainer.slideDown();
             chatContainer.toggleClass(classMaxim);
             if (chatContainer.hasClass(classMaxim)) {
             console.log('maximized...');
             chatContainer.draggable({
             handle: '.chat-header'
             });
             } else {
             console.log('restored...');
             chatContainer.draggable("destroy");
             chatContainer.removeAttr("style");
             }*/
        });
    }

    function loadMessages(pcantidad) {
        var cantidad = pcantidad || 10;
        var messages = [];
        for (var i = 0; i < cantidad; i++) {
            var text = getRandomTextMessage();
            var sent = (text.length % 2 === 0 && i > 0) ? true : false;
            messages.push(new CHAT_MESSAGE({sent: sent, text: text}));
        }
        return $q(function(resolve, reject) {
            resolve(messages);
        });
    }

    function getRandomTextMessage(pcantidad) {
        var words = ['ipsum dolor', 'sint occaecat', 'sit voluptatem', 'numquam eius modi', 'with source'];
        var text = "";
        var cantidad = pcantidad || 5;
        for (var m = 0; m < cantidad; m++) {
            text += words[Math.floor(Math.random() * words.length)] + " ";
        }
        return text;
    }

    function CHAT_MESSAGE(param) {

        this.sent = param.sent || false;
        this.text = param.text || "";
        this.author = param.author || vm.fn.getUserSelected();
        this.media = param.media || null;

        if (this.sent)
            this.avatar = "https://dummyimage.com/45x45.jpg/585959/f5f5f5";
        else
            this.avatar = "https://dummyimage.com/45x45.jpg/ccc";

    }

    //CHAT TABS
    function initChatTabs() {
        $(".chat-tabs").tabs({
            active: 0
        });
    }

    //CHAT OFFLINE-ONLINE SCROLL TOP
    function scrollTop() {

        var el = $('#chat-online-tab .chat-body-wrapper');
        var scrollHeight = el[0].scrollHeight;
        el.animate({
            scrollTop: scrollHeight
        });

        var el1 = $('#chat-offline-tab .chat-body-wrapper');
        var scrollHeight1 = el1[0].scrollHeight;
        el1.animate({
            scrollTop: scrollHeight1
        });
    }

    //CARGA MENSAJES OFFLINE
    function loadMessagesOffline() {

        var loadmessagesOffline = vm.fn.loadMessages();
        loadmessagesOffline.then(function(data) {

            vm.data.messagesoffline = data;

            $timeout(function() {
                scrollTop();
            });

        }, function(err) {
            console.log(err);
            alert('err cargando mensajes offline');
        });
    }

    //CARGA MENSAJES ONLINE
    function loadMessagesOnline() {
        var loadmessagesOnline = vm.fn.loadMessages();
        loadmessagesOnline.then(function(data) {

            vm.data.messagesonline = data;

            $timeout(function() {
                scrollTop();
            })

        }, function(err) {
            console.log(err);
            alert('err cargando mensajes onlin');
        });
    }

    //CHAT INPUT-OFFLINE
    function initChatInputOffline() {

        $('#input-chat-offline').chatInput({
            onKeyPress: function(evt, text) {
                if (evt.keyCode == 13 && text != '') {

                    var messageSent = new CHAT_MESSAGE({sent: true, text: text});

                    vm.data.messagesoffline.push(messageSent);

                    $timeout(function() {
                        scrollTop();
                    });

                    $timeout(function() {

                        var receivedText = getRandomTextMessage();
                        var messageSent = new CHAT_MESSAGE({text: receivedText});
                        vm.data.messagesoffline.push(messageSent);
                        scrollTop();

                    }, 3000);
                }
            }
        });

    }

    //CHAT INPUT-ONLINE
    function initChatInputOnline() {

        $('#input-chat-online').chatInput({
            onKeyPress: function(evt, text) {

                vm.fn.notifyTyping({text: text});

                if (evt.keyCode == 13 && text != '') {

                    vm.fn.renderMessage({sent: true, text: text});
                    vm.fn.sendMessage({text: text});

                    /*vm.data.messagesonline.push(message);
                     
                     $timeout(function() {
                     scrollTop();
                     });*/

                    /*$timeout(function() {
                     
                     var receivedText = getRandomTextMessage();
                     var messageSent = new CHAT_MESSAGE({text: receivedText});
                     vm.data.messagesonline.push(messageSent);
                     scrollTop();
                     
                     }, 3000);*/
                }
            }
        });

    }

    //CHAT TOOL-BTN
    function initChatToolsBtn() {
        $('.chat-toolbar-btn').on('click', function(evt) {
            if ($(this).hasClass('btn-close')) {

                var tabconent = $(this).closest('.chat-tab-content');

                var eloverlay = '<div class="chat-end-overlay inside"><p>Chat finalizado</p></di>';
                if (tabconent.find('.chat-end-overlay.inside').length > 0)
                    tabconent.find('.chat-end-overlay.inside').remove();
                else
                    tabconent.append(eloverlay);

            } else if ($(this).hasClass('btn-setting')) {
                alert('Not implemented yet...');
            }
        });
    }

    //CHAT FILE-INPUT
    function initChatFile() {
        $('#chatfile').chatFile({
            onSubmit: function(data) {

                if (data.base64 != null) {

                    var file = data.file;
                    var media = {
                        name: file.name,
                        size: file.size,
                        type: file.type,
                        base64: data.base64,
                        customtype: data.type
                    };

                    var mediaMessage = {sent: true, text: null, media: media};
                    vm.fn.renderMessage(mediaMessage);
                    vm.fn.sendMessage(mediaMessage);

                    $timeout(function() {
                        vm.img = data.base64;
                    })
                }
            }
        });
    }

    //CARGA LISTADO DE USUARIOS
    function loadUsers() {
        ChatSvc.users().then(function(data) {
            vm.data.users = data;
        }, function(err) {
            console.log('Error cargando usuarios: ', err);
        });
    }

    //RETORNA EL USUARIO SELECCIONADO
    function getUserSelected() {
        var user = 'https://dummyimage.com/45x45.jpg/ccc';
        if (angular.isDefined(vm.user.selected))
            user = vm.user.selected;
        return user;
    }
}

/*@ngInject*/
function ChatSvc($http, $q) {
    var service = {
        users: function() {

            var url = 'frontend/data/people.json';

            var req = {
                method: 'GET',
                url: url
            };

            function promise(resolve, reject) {
                $http(req).then(function(response) {
                    resolve(response.data);
                }, function(err) {
                    reject(err);
                })
            }
            return $q(promise);
        }
    }

    return service;
}
