(function(){

	"üse strict";

	angular.module('frontendApp')
	  .controller('RemoteCtrl', RemoteCtrl)
	  .service('RemoteSvc', RemoteSvc)
	  .directive('remoteSearch', RemoteSearchDirective)
	  .directive('intervalSelect', IntervalSelectDirective)


	  function RemoteCtrl($scope, RemoteSvc){

	  		var vm = this;
	  		vm.products = [];
	  		vm.q = "";
	  		vm.showSpinner = true;

			$scope.$watch("remote.q", WatchSearchRemote);

	  		RemoteSvc.findAll().then(okFunction, erFunction);


	  		/****** FUNCTIONS CALLBACK *********/

	  		function okFunction(response){
	  			vm.products = response.data;
	  		}

	  		function erFunction(response){
	  			alert("Ha ocurrido un error");
	  		}

			
			function WatchSearchRemote(newValue, oldValue) {
			  	if(newValue === oldValue)
			  		return;

			  	//RemoteSvc.filter(newValue).then(okFunction, erFunction);
			}	  		
	  }

	  /*@ngInject*/
	  function IntervalSelectDirective($interpolate){

	  		var directive = {
	  			restrict:'EA',
	  			template: function(){
	  				var tmpl = '';
	  				tmpl += '<ul class="interval-select">';

		  				tmpl += '<li>';
		  					tmpl += '<input ng-model="form.interval" type="number" placeholder="Intervalo" class="form-control" />';
		  				tmpl += '</li>';

		  				tmpl += '<li>';	  					  				
			  				tmpl += '<select class="form-control" ng-model="form.extension">';
			  				tmpl += '<option value="d">Día</option>';
			  				tmpl += '<option value="m">Mes</option>';
			  				tmpl += '<option value="y">Año</option>';
		  					tmpl += '</select>'
		  				tmpl += '</li>';

		  				tmpl += '<li>';	  					  				
			  				tmpl += '<button type="button" ng-click="fn.onSearch()" class="btn btn-primary"><i class="fa fa-search"></i></button>';
		  				tmpl += '</li>';		  				
	  				
	  				return tmpl;
	  			},
	  			link: function(scope, element, attrs){

	  				scope.fn = {
	  					onSearch: function(){

	  						var expParam = {
	  							interval: scope.form.interval || null,
	  							extension: scope.form.extension || null
	  						};

	  						if(expParam.interval == null || expParam.extension == null)
	  							return;

	  						var exp = $interpolate('{{interval}}{{extension}}');
	  						var interval = exp(expParam);
	  						scope.interval = interval;
	  						scope.onSearch({interval: interval});
	  					},
	  					setIntervalForm: function(pinterval){

	  						var interval = pinterval || null;

	  						if(interval != null && angular.isDefined(interval)){

	  							var i = interval.substring(0, interval.length - 1);
		  						var e = interval.substring(interval.length - 1);		  						

				  				scope.form = {
				  					interval: parseInt(i),
				  					extension: e.toLowerCase()
				  				};
	  						}
	  					}	
	  				};

	  				scope.fn.setIntervalForm(scope.interval);
	  			},
	  			scope:{
	  				onSearch: '&',
	  				interval: '=' 
	  			}
	  		};

	  		return directive;

	  }

	  function RemoteSvc($http){

		function appendTransform(defaults, transform) {
		  defaults = angular.isArray(defaults) ? defaults : [defaults];
		  return defaults.concat(transform);
		}

		function PRODUCTO(model){
			this.id = model.id || null;
			this.name = model.name || "";
			this.price = model.price || 0;
			this.date = new Date();
		}

		function doTransform(value){
			for (var i = value.length - 1; i >= 0; i--) 
				 value[i] = new PRODUCTO(value[i]);

			return value;
		}

		var _request = {
			 metho: 'GET',
			 url: 'http://example.com',
		  	 transformResponse: appendTransform($http.defaults.transformResponse, function(value) {
		    	return doTransform(value);
		  	  })			 
		};

		var baseUrl = "http://localhost:3000";
		var remoteShopUrl = baseUrl + "/shops";

	  	var service = {
	  		findAll: function(){
	  			_request.url = remoteShopUrl;
	  			return $http(_request);
	  		},
	  		filter: function(filter){
	  			_request.url = remoteShopUrl + "/" + filter;
	  			return $http(_request);
	  		}
	  	}

	  	return service;
	  }

	  function RemoteSearchDirective(RemoteSvc){
	  		var directive = {
	  			restrict:"A",
	  			require: 'ngModel',
	  			link: function(scope, element, attrs, ngModel){
		           
		           /*scope.$watch(function () {
		              return ngModel.$modelValue;
		           }, function(newValue) {
		               console.log(newValue);
		           });*/

			  		function okFunction(response){
			  			scope.collection = response.data || [];
			  		}

			  		function erFunction(response){
			  			alert("Ha ocurrido un error");
			  		}

					
					function WatchSearchRemote(newValue, oldValue) {
					  	if(newValue === oldValue)
					  		return;

					  	RemoteSvc.filter(newValue).then(okFunction, erFunction);
					}	

		           scope.$watch("text", WatchSearchRemote);

	  			},
	  			scope: {
	  						text:"=ngModel", 
	  						collection:"=?"
	  					}
	  		};
	  		return directive;
	  }

})();