'use strict';

/**
 * @ngdoc overview
 * @name frontendApp
 * @description
 * # frontendApp
 *
 * Main module of the application.
 */
angular
        .module('frontendApp', [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
            'angularSpinner',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'ui.select',
            'mgcrea.ngStrap',
            'toastr',
            'LocalStorageModule',
            'ngWizard',
            'ngScrollbar'
        ])
        .config(function($routeProvider) {
            $routeProvider
                    .when('/', {
                        templateUrl: 'frontend/app/views/main.html',
                        controller: 'MainCtrl',
                        controllerAs: 'main'
                    })
                    .when('/login', {
                        templateUrl: 'frontend/app/views/login.html',
                        controller: 'LoginCtrl',
                        controllerAs: 'vm'
                    })
                    .when('/about', {
                        templateUrl: 'frontend/app/views/about.html',
                        controller: 'AboutCtrl',
                        controllerAs: 'about'
                    })
                    .when('/contact', {
                        templateUrl: 'frontend/app/views/contact.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'contact'
                    })
                    .when('/remote', {
                        templateUrl: 'frontend/app/views/remote.html',
                        controller: 'RemoteCtrl',
                        controllerAs: 'remote'
                    })
                    .when('/conciliador', {
                        templateUrl: 'frontend/app/views/conciliador/create.html',
                        controller: 'ConciliadorCtrl',
                        controllerAs: 'vm'
                    })
                    .when('/chat', {
                        templateUrl: 'frontend/app/views/chat.html',
                        controller: 'ChatCtrl',
                        controllerAs: 'vm',
                        resolve: {
                            socket: function() {
                                return io.connect('http://localhost:3000', {'forceNew': true});
                            }
                        }
                    })
                    .when('/profile', {
                        templateUrl: 'frontend/app/views/profile.html',
                        controller: 'ProfileCtrl',
                        controllerAs: 'vm'
                    })
                    .otherwise({
                        redirectTo: '/'
                    });
        })
        .config(['usSpinnerConfigProvider', function(usSpinnerConfigProvider) {
                usSpinnerConfigProvider.setDefaults({radius: 10, width: 3, length: 5});
            }])
        .config(function(localStorageServiceProvider) {

            localStorageServiceProvider
                    .setPrefix('chat_');

            localStorageServiceProvider
                    .setStorageType('sessionStorage');

        })
        .config(function(toastrConfig) {
          angular.extend(toastrConfig, {
            toastClass: 'mcui-toast toast',
            extraData: {
                msgBodyClass: 'toast-custom-body'
            },          
            templates: {
              toast: 'frontend/app/widgets/toastr/toast.html'
            }          
          });
        })        
        .constant('uiDatetimePickerConfig', {
            dateFormat: 'yyyy-MM-dd HH:mm',
            defaultTime: '00:00:00',
            html5Types: {
                date: 'yyyy-MM-dd',
                'datetime-local': 'yyyy-MM-ddTHH:mm:ss.sss',
                'month': 'yyyy-MM'
            },
            initialPicker: 'date',
            reOpenDefault: false,
            enableDate: true,
            enableTime: true,
            buttonBar: {
                show: true,
                now: {
                    show: true,
                    text: 'Ahora'
                },
                today: {
                    show: true,
                    text: 'Hoy'
                },
                clear: {
                    show: true,
                    text: 'Reiniciar'
                },
                date: {
                    show: true,
                    text: 'Fecha'
                },
                time: {
                    show: true,
                    text: 'Hora'
                },
                close: {
                    show: true,
                    text: 'Cerrar'
                }
            },
            closeOnDateSelection: true,
            closeOnTimeNow: true,
            appendToBody: false,
            altInputFormats: [],
            ngModelOptions: {},
            saveAs: false,
            readAs: false,
        })
        .constant('USER_AUTH_KEY', 'chatUserAuth')
        .directive('mcuiFormInput', McuiFormInput)
        .directive('mcuiOnlyNumbers', McuiOnlyNumbers)
        .directive('mcuiFavourite', McuiFavourite)
        .directive('mcuiWish', McuiWish)
        .directive('mcuiMap', McuiMap)
        .directive('mcuiBarRating', McuiBarRating)
        .directive('mcuiModal', McuiModal)
        .service('McuiModalSvc', McuiModalSvc);    

/*@ngInject*/
function McuiModalSvc($rootScope, $document, $compile){
    
    var scope = $rootScope.$new();

    var service = {
        show: function(data){

            var init = {
                scope: scope,
                templateUrl: 'frontend/app/widgets/cmp.modal.template.html' 
            };

            var customData = {};

            //extending properties
            angular.extend(customData, init, data);

            //preload customData
            customData.scope.templateUrl = customData.templateUrl;

            //append modal to body element
            var body = $document.find('body').eq(0);
            body.append($compile('<mcui-modal></mcui-modal')(customData.scope));            
        },
        closeAll: function(){
            $document.find('.mcui-modal').remove();
            scope.$destroy();
        }
    };

    return service;
}

/*@ngInject*/
function McuiModal($document){
    return {
        restrict:'EA',
        templateUrl: function(){
            return 'frontend/app/widgets/cmp.modal.html';
        },
        link: function(scope, element, attrs){
            scope.closeThisModal = function(){
                $(element).remove();
                scope.$destroy();
            }
        }
    }
}

/*@ngInject*/
function McuiFormInput() {

    var templateFn = function(element, attrs) {

        var errorClassName = "'has-error'";
        var labelName = attrs.label || '';
        var inputType = attrs.type || 'text';
        var fieldModelName = getFieldModelName(attrs);

        var minlength = attrs.minlength || '';  //r_minlength
        var maxlength = attrs.maxlength || '';  //r_maxlength
        var readonly = attrs.readonly || '';    //r_readonly
        var disabled = attrs.disabled || '';    //r_disabled

        var minvalue = attrs.min || '';         //r_min
        var maxvalue = attrs.max || '';         //r_max
        var step = attrs.step || '';            //r_step

        var pattern = attrs.pattern || '';      //r_pattern

        var rutFormat = attrs.rutFormat || 'live'; //r_rutformat => blur | live

        //input text
        var itext = '<input';
        itext += ' type="inputType" name="fieldModelName" ng-model="ngModel"';
        itext += ' class="form-control"';
        itext += ' placeholder="{{ placeholder }}"';
        itext += ' ng-required="required"';
        itext += ' ng-minlength="r_minlength" ';
        itext += ' ng-maxlength="r_maxlength"';
        itext += ' ng-min="r_minvalue"';
        itext += ' ng-max="r_maxvalue"';
        itext += ' ng-step="r_step"';
        itext += ' ng-readonly="r_readonly"';
        itext += ' ng-disabled="r_disabled"';
        itext += ' ng-pattern="r_pattern" />';

        //input rut
        var irut = '<input';
        irut += ' type="text" name="fieldModelName" ng-model="ngModel" ng-rut rut-format="r_rutformat"';
        irut += ' class="form-control"';
        irut += ' ng-required="required"';
        irut += ' ng-readonly="r_readonly"';
        irut += ' ng-disabled="r_disabled"';
        irut += ' placeholder="{{ placeholder }}" />';

        //input phone
        var iphone = '<input';
        iphone += ' type="text" name="fieldModelName" ng-model="ngModel" mcui-only-numbers';
        iphone += ' class="form-control"';
        iphone += ' placeholder="{{ placeholder }}"';
        iphone += ' ng-required="required"';
        iphone += ' ng-minlength="r_minlength" ';
        iphone += ' ng-maxlength="r_maxlength"';
        iphone += ' ng-readonly="r_readonly"';
        iphone += ' ng-disabled="r_disabled"';
        iphone += ' ng-pattern="r_pattern" />';

        //input type lower
        var itype = inputType.toLowerCase();

        //input type checking type to replace
        var itemplate = "";

        switch (itype) {
            case 'rut':
                itemplate = irut;
                break;
            case 'phone':
                itemplate = iphone;
                break;
            default:
                itemplate = itext;
        }

        //ivalidate
        var vtemplate = '';
        vtemplate += '<span ng-show="form.fieldModelName.$error.required && showErrorMessage == true && form.fieldModelName.$dirty" class="help-block">Campo obligatorio</span>';
        vtemplate += '<span ng-show="form.fieldModelName.$error.email && showErrorMessage == true" class="help-block">Formato de correo inválido</span>';
        vtemplate += '<span ng-show="form.fieldModelName.$error.url && showErrorMessage == true" class="help-block">Formato de url inválido</span>';
        vtemplate += '<span ng-show="form.fieldModelName.$error.min && showErrorMessage == true" class="help-block">Valor mínimo (r_minvalue)</span>';
        vtemplate += '<span ng-show="form.fieldModelName.$error.max && showErrorMessage == true" class="help-block">Valor máximo (r_maxvalue)</span>';
        vtemplate += '<span ng-show="form.fieldModelName.$error.pattern && showErrorMessage == true" class="help-block">Formato inválido</span>';
        vtemplate += '<span ng-show="form.fieldModelName.$error.minlength && showErrorMessage == true" class="help-block">Tamaño mínimo (r_minlength)</span>';
        vtemplate += '<span ng-show="form.fieldModelName.$error.maxlength && showErrorMessage == true" class="help-block">Tamaño máximo (r_maxlength)</span>';
        vtemplate += '<span ng-show="form.fieldModelName.$error.rut && showErrorMessage == true" class="help-block">Formato RUT inválido</span>';

        //input form template
        var template = '<div class="form-group" ng-class="{errorClassName: form.fieldModelName.$invalid}">';
        template += '<label>{{ label }}</label>' + itemplate + vtemplate + '</div>';

        var templateReplaced = template
                .replace("errorClassName", errorClassName)
                .replace("labelName", labelName)
                .replace("inputType", inputType)
                .replace(/fieldModelName/g, fieldModelName)
                .replace(/r_minlength/g, minlength)
                .replace(/r_maxlength/g, maxlength)
                .replace(/r_pattern/g, pattern)
                .replace(/r_minvalue/g, minvalue)
                .replace(/r_maxvalue/g, maxvalue)
                .replace(/r_rutformat/g, rutFormat)
                .replace(/r_step/g, step)
                .replace(/r_readonly/g, readonly)
                .replace(/r_disabled/g, disabled);

        return templateReplaced;
    };

    function getFieldModelName(attrs) {
        var objectAndField = attrs.ngModel;
        var names = objectAndField.split('.');

        var fieldModelName = 'input_';
        if (names.length == 0)
            fieldModelName = objectAndField;
        else
            fieldModelName = names[names.length - 1];

        return fieldModelName;
    }

    var directive = {
        restrict: "EA",
        replace: false,
        require: ['^form', 'ngModel'],
        template: templateFn,
        link: function(scope, element, attrs, ctrls) {

            var properties = (typeof attrs.input != 'undefined') ? scope.$eval(attrs.input) : {};
            var propertiesLabel = (typeof attrs.labelProp != 'undefined') ? scope.$eval(attrs.labelProp) : {};

            scope.form = ctrls[0];
            var ngModel = ctrls[1];

            scope.required = (typeof attrs['required'] != 'undefined' || typeof properties['required'] != 'undefined' || typeof properties['ng-required'] != 'undefined') ? true : false;
            scope.placeholder = (typeof attrs['placeholder'] != 'undefined') ? attrs.placeholder : typeof properties['placeholder'] != 'undefined' ? properties['placeholder'] : '';
            scope.showErrorMessage = (typeof attrs['showErrorMessage'] != 'undefined' || typeof properties['showErrorMessage'] != 'undefined') ? true : false;

            for (var prop in properties)
                if (prop != 'placeholder' && prop != 'required' && prop != 'ng-required' && prop != 'showErrorMessage')
                    element.find('.form-control').prop(prop, properties[prop]);

            for (var prop in propertiesLabel)
                element.find('label').prop(prop, propertiesLabel[prop]);

            var fieldModelName = getFieldModelName(attrs);
            scope.$watch(fieldModelName, function() {
                ngModel.$setViewValue(scope[fieldModelName]);
            });
        },
        scope: {
            ngModel: "=",
            label: "@",
            input: "@"
        }
    };

    return directive;
}

/*@ngInject*/
function McuiOnlyNumbers() {

    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {

            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }

            ngModelCtrl.$parsers.push(fromUser);
        }
    }
}

/*@ngInject*/
function McuiFavourite() {

    var directive = {
        restrict: 'EA',
        template: function(element, attrs) {
            var template = '<button class="btn-favourite"><i class="fa" ng-class="checked == true ? \'fa-star\':  \'fa-star-o\' "></i></button';
            return template;
        },
        link: function(scope, element, attrs) {
            element.on('click', function() {
                scope.checked = !scope.checked;
            })
        },
        scope: {
            checked: '='
        }
    };

    return directive;
}

/*@ngInject*/
function McuiWish($http, $timeout) {

    var directive = {
        restrict: 'EA',
        template: function(element, attrs) {
            var template = '<button class="btn-wish"><i class="fa" ng-class="checked == true ? \'fa-heart\':  \'fa-heart-o\' "></i></button';
            return template;
        },
        link: function(scope, element, attrs) {
            element.on('click', function() {
                scope.checked = !scope.checked;
            })
        },
        scope: {
            checked: '='
        }
    };

    return directive;
}

/*@ngInject*/
function McuiMap($http, $timeout) {

    var directive = {
        restrict: 'EA',
        replace: false,
        templateUrl: function(element, attrs) {
            return 'frontend/app/views/map.tmpl.html';
        },
        link: function(scope, element, attrs) {

            $timeout(function() {



            })

            var map;
// JSON con la información de las ciudades
            var jsonString =
                    '{"Madrid":{"Position":{"Longitude":40.405131,"Latitude":-3.724365}},"Barcelona":{"Position":{"Longitude":41.525030,"Latitude":2.449951}},"Bilbao":{"Position":{"Longitude":43.237199,"Latitude":-2.922363}},"A Coruña":{"Position":{"Longitude":43.357138,"Latitude":-8.415527}},"Granada":{"Position":{"Longitude":37.177826,"Latitude":-3.592529}},"Alicante":{"Position":{"Longitude":38.333039,"Latitude":-0.483398}},"Vigo":{"Position":{"Longitude":42.228517,"Latitude":-8.701172}},"Málaga":{"Position":{"Longitude":36.71246,"Latitude":-4.427490}},"Santander":{"Position":{"Longitude":43.460894,"Latitude":-3.812256}},"Badajoz":{"Position":{"Longitude":38.865375,"Latitude":-6.976318}}}';
            var myData = JSON.parse(jsonString);
            function error(msg) {
                alert("error" + msg);
            }
// Obtenemos la posición del usuario y lo manejamos con la función initialize
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(initialize, error, {
                    maximumAge: 60000,
                    timeout: 4000
                });
            } else {
                error('Actualiza el navegador web para usar el API de localización');
            }
            function initialize(position) {
// Inicializamos las opciones del mapa
                var mapOptions = {
                    zoom: 6,
// Establecemos la posición actual como centro
                    center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
// Este es el estilo proporcionado por Snazzy Maps.
                    styles: [{
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 17
                                }]
                        }, {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 20
                                }]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 17
                                }]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 29
                                }, {
                                    "weight": 0.2
                                }]
                        }, {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 18
                                }]
                        }, {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 16
                                }]
                        }, {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 21
                                }]
                        }, {
                            "elementType": "labels.text.stroke",
                            "stylers": [{
                                    "visibility": "on"
                                }, {
                                    "color": "#000000"
                                }, {
                                    "lightness": 16
                                }]
                        }, {
                            "elementType": "labels.text.fill",
                            "stylers": [{
                                    "saturation": 36
                                }, {
                                    "color": "#000000"
                                }, {
                                    "lightness": 40
                                }]
                        }, {
                            "elementType": "labels.icon",
                            "stylers": [{
                                    "visibility": "off"
                                }]
                        }, {
                            "featureType": "transit",
                            "elementType": "geometry",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 19
                                }]
                        }, {
                            "featureType": "administrative",
                            "elementType": "geometry.fill",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 20
                                }]
                        }, {
                            "featureType": "administrative",
                            "elementType": "geometry.stroke",
                            "stylers": [{
                                    "color": "#000000"
                                }, {
                                    "lightness": 17
                                }, {
                                    "weight": 1.2
                                }]
                        }],
                    disableDefaultUI: true,
                };
                // Cargamos el mapa en el contenedor html creado.
                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                // Añadimos un marcador a la posición actual
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                    map: map,
                    title: "Usted está aquí."
                });
                var distanceObj = [],
                        i = 0;
                // Calculamos la distancia entre los puntos
                $.each(myData, function(a, b) {
                    distanceObj[i] = {
                        distance: coordsDistance(position.coords.latitude, position.coords.longitude, b.Position.Longitude, b.Position.Latitude),
                        location: b,
                        city: a
                    };
                    ++i;
                });

                // Ordenamos los elementos del array por el valor distancia obtenido
                distanceObj.sort(function(a, b) {
                    return parseInt(a.distance) - parseInt(b.distance)
                });

                // Pintamos cada uno de los lugares
                $.each(distanceObj, function(a, b) {
                    $('#markers ul').append('<li>' + b.city + ': ' + b.distance + 'KM </li>');
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(b.location.Position.Longitude, b.location.Position.Latitude),
                        map: map,
                    });
                    marker['infowindow'] = new google.maps.InfoWindow({
                        content: b.city,
                        maxWidth: 500,
                        width: 500
                    });
                    google.maps.event.addListener(marker, 'mouseover', function() {
                        marker['infowindow'].open(map, marker);
                    });
                    google.maps.event.addListener(marker, 'mouseout', function() {
                        marker['infowindow'].close();
                    });
                });
            }

            // Función que calcula la distancia entre dos coordenadas devuelta en KM
            function coordsDistance(pmeineLongitude, pmeineLatitude, long1, lat1) {
                var erdRadius = 6371;
                var meineLongitude = pmeineLongitude * (Math.PI / 180);
                var meineLatitude = pmeineLatitude * (Math.PI / 180);
                long1 = long1 * (Math.PI / 180);
                lat1 = lat1 * (Math.PI / 180);
                var x0 = meineLongitude * erdRadius * Math.cos(meineLatitude);
                var y0 = meineLatitude * erdRadius;
                var x1 = long1 * erdRadius * Math.cos(lat1);
                var y1 = lat1 * erdRadius;
                var dx = x0 - x1;
                var dy = y0 - y1;
                var d = Math.sqrt((dx * dx) + (dy * dy));
                return Math.round(d);
            }
        },
        scope: {}
    };

    return directive;
}

/*@ngInject*/
function McuiBarRating($http, $timeout) {

    function McuiBarRatingLink(scope, element, attrs) {

        var attrsSettings = {};

        var settingsAvailable = [
            'theme',
            'initialRating',
            'allowEmpty',
            'emptyValue',
            'showValues',
            'showSelectedRating',
            'deselectable',
            'reverse',
            'readonly',
            'fastClicks',
            'hoverState',
            'silent'
        ];

        for (var att in attrs) {
            var index = settingsAvailable.indexOf(att);
            if (index != -1)
                attrsSettings[settingsAvailable[index]] = scope.$eval(attrs[att]); 
        }

        var defaultSettings = {
            //Defines a theme.
            theme: 'fontawesome-stars',
            //Defines initial rating.
            initialRating: null,
            //If set to true, users will be able to submit empty ratings.
            allowEmpty: null,
            //Defines a value that will be considered empty. It is unlikely you will need to modify this setting.
            emptyValue: '',
            //If set to true, rating values will be displayed on the bars.
            showValues: false,
            //If set to true, user selected rating will be displayed next to the widget.
            showSelectedRating: true,
            //If set to true, users will be able to deselect ratings.
            deselectable: true,
            //If set to true, the ratings will be reversed.
            reverse: false,
            //If set to true, the ratings will be read-only.
            readonly: false,
            //Remove 300ms click delay on touch devices.
            fastClicks: true,
            //Change state on hover.
            hoverState: true,
            //Supress callbacks when controlling ratings programatically.
            silent: false
        };

        var defaultEvents = {
            onSelect: function(value, text, event) {
                if (typeof (event) !== 'undefined') {
                    // rating was selected by a user
                    //console.log(event.target);
                    if (typeof scope.onSelect == 'function')
                        scope.onSelect({value: value, text: text, event: event});
                } else {
                    // rating was selected programmatically
                    // by calling `set` method
                }
            },
            onClear: function(value, text) {
                if (typeof scope.onClear == 'function')
                    scope.onClear({value: value, text: text});
            },
            onDestroy: function(value, text) {
                if (typeof scope.onDestroy == 'function')
                    scope.onDestroy({value: value, text: text});
            }
        };

        var settings = angular.extend({}, defaultSettings, attrsSettings, defaultEvents);
        
        scope.$watch('ngModel', function(newValue, oldValue) {
            $timeout(function() {
                element.barrating('set', newValue);
            })
        });

        $timeout(function() {
            element.barrating(settings);
        });
    }

    function McuiTemplateFunc(element, attrs) {
        var tmpl = '';
        tmpl += '<select>';
        tmpl += '<option value="1">1</option>';
        tmpl += '<option value="2">2</option>';
        tmpl += '<option value="3">3</option>';
        tmpl += '<option value="4">4</option>';
        tmpl += '<option value="5">5</option>';
        tmpl += '</select>';

        return tmpl;
    }

    return {
        restrict: 'E',
        replace: true,
        link: McuiBarRatingLink,
        template: McuiTemplateFunc,
        scope: {
            ngModel: '=?',
            onSelect: '&',
            onClear: '&',
            onDestroy: '&'
        }
    };
}