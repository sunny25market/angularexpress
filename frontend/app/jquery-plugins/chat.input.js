jQuery.fn.chatInput = function(options) {

    var vm = this;

    //---PLUGIN FUNCTIONS
    vm.fn = {
        getRandomKey: getRandomKey
    };

    //---DEFAULT SETTINGS
    vm.defaults = {
        inputRef: vm.fn.getRandomKey(),
        onKeyPress: null,
        inputClass: 'chat-input-textarea',
        minHeight: 30,
        maxHeight: 60
    };

    //---EXTENDING DEFAULT SETTINGS
    var settings = $.extend({}, vm.defaults, options);

    //---FUNCTIONS
    function getRandomKey() {
        var text = "chihidden_";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    return this.each(function() {

        var element = $(this);
        var inputRef = settings.inputRef;
        var input = '<input type="text" id="' + inputRef + '" style="display:none" />';
        var inputID = "#" + inputRef;
       
        $(element).addClass(settings.inputClass);
        $(element).attr('contenteditable', true);
        $(input).insertAfter(element);
        if (typeof settings.onKeyPress == 'function') {
            $(this).keypress(function(evt) {
                settings.onKeyPress(evt, $(inputID).val());
                if (evt.keyCode == 13) {
                    evt.preventDefault();
                    $(element).empty();
                    $(inputID).val("");
                    $(element).css({height: settings.minHeight});
                }
            });
        }

        $(element).bind('input propertychange', function(evt) {

            var prev = $(inputID);
            var nextVal = element[0].textContent;
            var prevVal = prev.val();
            var clientHeight = element[0].clientHeight;
            var scrollHeight = element[0].scrollHeight;
            var hasScroll = scrollHeight > clientHeight;
            var initialHeight = settings.minHeight;
            var maxHeight = settings.maxHeight;
            if (nextVal.length > prevVal.length) {

                var calulatedHeight = parseInt(clientHeight) + parseInt(initialHeight);
                while (hasScroll && clientHeight <= maxHeight) {
                    element.css({'height': calulatedHeight});
                    hasScroll = element[0].scrollHeight > element[0].clientHeight;
                    clientHeight = element[0].clientHeight;
                    calulatedHeight = parseInt(clientHeight) + parseInt(initialHeight);
                }

            } else if (nextVal.length == prevVal.length) {
                //console.log("no change..");
            } else if (nextVal.length < prevVal.length) {

                if (hasScroll) {


                } else {

                    var flagStop = false;
                    while (!hasScroll && clientHeight >= initialHeight) {

                        var calulatedHeight = parseInt(clientHeight) - parseInt(initialHeight);
                        element.css({'height': calulatedHeight});
                        hasScroll = element[0].scrollHeight > element[0].clientHeight;
                        clientHeight = element[0].clientHeight;
                        if (hasScroll) {

                            var newHeight = parseInt(calulatedHeight) + (parseInt(initialHeight));
                            if (clientHeight < initialHeight) {
                                element.css({'height': initialHeight});
                            } else {
                                element.css({'height': newHeight});
                            }

                            flagStop = true;
                        }
                    }
                }
            }
            prev.val(nextVal);
        });
    });
};