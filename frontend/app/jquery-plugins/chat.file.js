jQuery.fn.chatFile = function(options) {

    var vm = this;

    //---PLUGIN FUNCTIONS
    vm.fn = {
        getRandomKey: getRandomKey,
        getFileTemplate: getFileTemplate,
        showHidePreview: showHidePreview,
        getBase64: getBase64,
        getFileType: getFileType,
        isFileAllowed: isFileAllowed
    };

    //---DEFAULT SETTINGS
    vm.defaults = {
        formRef: vm.fn.getRandomKey(),
        inputRef: vm.fn.getRandomKey(),
        inputName: 'file',
        fileViewer: '#image-upload',
        onSend: null,
        onSubmit: null,
        noimage: 'frontend/app/images/no_image.png',
        docimage: 'frontend/app/images/doc_image.png',
        pdfimage: 'frontend/app/images/pdf_image.png',
        xlsimage: 'frontend/app/images/xls_image.png'
    };

    //---GLOBAL SETTINGS
    vm.global = {
        toolbarRef: vm.fn.getRandomKey(),
        fileAllow: {
            image: ['image/png', 'image/jpg', 'image/jpeg'],
            doc: [],
            pdf: ['application/pdf'],
            xls: ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
        },
        types: {
            doc: 'doc',
            image: 'image',
            pdf: 'pdf',
            xls: 'xls'
        }
    };

    //---EXTENDING DEFAULT SETTINGS
    var settings = $.extend({}, vm.defaults, options, vm.global);


    //---FUNCTIONS
    function getRandomKey() {
        var text = "chfile_";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 8; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    function getFileTemplate(target, file) {

        var src = settings.noimage;

        var type = vm.fn.getFileType(file);
        var types = settings.types;
        if (type === types.image)
            src = target.result;

        else if (type === types.pdf)
            src = settings.pdfimage;

        else if (type === types.doc)
            src = settings.docimage;

        else if (type === types.xls)
            src = settings.xlsimage;

        var imagePreview = '<img src="' + src + '" style="height: 150px;" id="image" />';
        return imagePreview;
    }

    function showHidePreview(el, display) {
        var show = display || false;
        if (show == true)
            $(el).show();
        else
            $(el).hide();
    }

    function getBase64(file, onload, onerror) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(evt) {
            var binaryString = evt.target.result;
            console.log('file/loading...');
            if (typeof onload == 'function')
                onload({base64: binaryString, err: false, message: 'ok'});
        };
        reader.onerror = function(error) {
            console.log('file/error...', error);
            if (typeof onerror == 'function')
                onerror({base64: null, err: true, message: error});
        };
    }

    function getFileType(file) {

        var types = settings.types;

        var fileType = file.type || null;

        if (settings.fileAllow.image.indexOf(fileType) != -1)
            return types.image;
        else if (settings.fileAllow.pdf.indexOf(fileType) != -1)
            return types.pdf;
        else if (settings.fileAllow.doc.indexOf(fileType) != -1)
            return types.doc;
        else if (settings.fileAllow.xls.indexOf(fileType) != -1)
            return types.xls;

        return null;
    }

    function isFileAllowed(file) {
        var allow = settings.fileAllow;
        var result = [];
        for (var prop in allow)
            for (var i = 0; i < (allow[prop]).length; i++)
                result.push(allow[prop][i]);

        var isAllow = result.indexOf(file.type) != -1 ? true : false;
        return isAllow;
    }

    return this.each(function() {

        var element = $(this);
        var formRef = 'frm' + settings.formRef;
        var inputRef = settings.inputRef;
        var inputName = settings.inputName;
        var inputHandler = "#" + inputRef;

        //input file elements
        var elFileForm = '<form id="' + formRef + '" name="' + formRef + '" method="post" enctype="multipart/form-data"></form>';
        var elFileInput = '<input type="file" id="' + inputRef + '" name="' + inputName + '" class="inputfile" data-multiple-caption="{count}" />';
        var elFileLabel = '<label for="' + inputRef + '"><i class="fa fa-picture-o"></i></label>';

        //file privew elements
        var elPreToolbar = '';
        elPreToolbar += '<div class="chat-img-toolbar"">';
        elPreToolbar += '  <span class="chat-img-tool-btn btn-tool-delete" title="Cancelar"><i class="fa fa-trash-o"></i></span>';
        elPreToolbar += '  <span class="chat-img-tool-btn btn-tool-upload" title="Enviar"><i class="fa fa-upload"></i></span>';
        elPreToolbar += '</div>';

        //hide preview layout
        vm.fn.showHidePreview(settings.fileViewer);

        //append input
        element
                .append($(elFileForm));

        //append input
        element
                .find('form#' + formRef)
                .append($(elFileInput));

        //append label
        element
                .find('form#' + formRef)
                .append($(elFileLabel));

        //preparing event input        
        $(inputHandler).on('change', function(evt) {

            if (!this.files || this.files.length == 0 || !vm.fn.isFileAllowed(this.files[0]))
                return;

            var files = this.files[0];

            var reader = new FileReader();

            reader.onload = function(e) {

                //clean and build
                $(settings.fileViewer)
                        .empty()
                        .append($(elPreToolbar))
                        .append($(vm.fn.getFileTemplate(e.target, files)));
                
                vm.fn.showHidePreview(settings.fileViewer, true);
            };

            reader.onloadend = function(e) {

            }

            reader.onloadstart = function(e) {

            }

            reader.onprogress = function(e) {

            }

            reader.onabort = function(e) {

            }

            reader.onerror = function(e) {

            }

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        })

        //preparing event file/delete
        $(settings.fileViewer).on('click', '.btn-tool-delete', function(evt) {
            $(settings.fileViewer).empty();
            vm.fn.showHidePreview(settings.fileViewer);
            $(inputHandler).val(null);
        })

        //preparing event file/send
        $(settings.fileViewer).on('click', '.btn-tool-upload', function(evt) {
            $(settings.fileViewer).empty();
            vm.fn.showHidePreview(settings.fileViewer);

            if (typeof settings.onSubmit == 'function') {
                var file = document.querySelector('input[type="file"]' + inputHandler).files[0];
                vm.fn.getBase64(file, function(data) {
                    settings.onSubmit({id: formRef, base64: data.base64, file: file, type: vm.fn.getFileType(file)});
                });
            }
            $(inputHandler).val(null);
        })
    });
};