var express = require('express');
//require('array.prototype.find');
var router = express.Router();

var products = [{id:1, name:'Apple', price:2000}, {id:2, name:'Orange', price:3000}, {id:3, name:'Banana', price:3000}];


/***************************    FIND-ALL */

exports.findAll = function(req, res) {
    res.json(products);
};

/***************************    FIND */
exports.find = function(req, res) {

	function findProduct(product){
		var f = req.params.filter;
		return product.id == f || product.price == f || product.name == f;
	}

	var producto = products.find(findProduct);

	res.json(producto);
};

/***************************    FILTER */

exports.filter = function(req, res) {

	console.log("FILTER: ", req.params.filter);

	function findProduct(product){
		var f = req.params.filter;
		return product.price == f || product.name == f;
	}

	var producto = products.filter(findProduct);

	res.json(producto);
};




